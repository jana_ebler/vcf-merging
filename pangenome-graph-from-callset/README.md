# Building a Pangenome graph from a reference panel VCF #

This pipeline produces a fully-phased VCF file representing a pangenome graph from a callset VCF containing fully-phased haplotypes (=reference panel).
All clusters of variants with overlapping coordinates are merged to form the bubbles of the pangenome graph. Each record in the resulting VCF will
represent a bubble.

## Required input data

* a reference genome in FASTA format
* the input VCF (fully-phased, multi-sample VCF representing a reference panel)

## How to run

* insert paths to the input data (reference genome, VCF) into the config file (`` contig.yaml ``)
* run pipeline: `` snakemake -j <n_cores>`` 

## Output

The pangenome graph is represented in terms of a fully-phased, multi-sample VCF file containing one record per bubble:

* `` <outdir>/pangenome/pangenome.vcf ``
