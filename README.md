# Pipelines for Pangenome graph construction #

* `` pangenome-graph-from-assemblies/ `` contains a pipeline for generating a pangenome graph from haplotype-resolved assemblies
* `` pangenome-graph-from-callset/ `` contains a pipeline for generating a pangenome graph from a given callset VCF

For more information, check the corresponding README files.